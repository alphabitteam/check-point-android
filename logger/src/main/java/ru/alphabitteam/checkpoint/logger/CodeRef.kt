package ru.alphabitteam.checkpoint.logger

import com.annimon.stream.Collectors
import com.annimon.stream.Stream

/**
 * @author Yuri Zigunov (fly1232008@yandex.ru)
 * @since 26.05.2019.
 */
class CodeRef(private val codeReference: String) {

    fun codeReference(): String {
        return codeReference
    }

    override fun toString(): String {
        return codeReference()
    }

    companion object {

        private fun codeReference(stackTrace: Array<StackTraceElement>, dropStackElementCount: Int): String {
            return Stream
                .of(*stackTrace)
                .map{it.toString()}
                .skip(dropStackElementCount.toLong())
                .limit(8)
                .collect(Collectors.joining("\n", "---Source code reference:\n", "\n---end"))
        }

        @JvmOverloads
        @JvmStatic
        fun x(dropStackElementCount: Int = 1): CodeRef {
            if (dropStackElementCount < 0) {
                throw IllegalArgumentException("dropStackElementCount must be >= 0")
            }

            val tracingException = Exception()
            val stackTrace = tracingException.stackTrace
            val codeReference = codeReference(stackTrace, dropStackElementCount + 1)
            return CodeRef(codeReference)
        }
    }
}