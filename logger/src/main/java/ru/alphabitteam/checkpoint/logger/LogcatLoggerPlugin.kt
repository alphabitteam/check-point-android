package ru.alphabitteam.checkpoint.logger

import android.util.Log
import java.util.HashMap

/**
 * @author Yuri Zigunov (fly1232008@yandex.ru)
 * @since 27.05.2019.
 */
class LogcatLoggerPlugin : LoggerPlugin(BuildConfig.DEBUG, LoggerPriority.VERBOSE) {

    private val priorityLoggers: MutableMap<LoggerPriority, (String, String) -> Unit>

    init {
        priorityLoggers = HashMap()
        priorityLoggers[LoggerPriority.VERBOSE] = {s, s2 -> Log.v(s, s2) }
        priorityLoggers[LoggerPriority.DEBUG] =  {s, s2 -> Log.d(s, s2) }
        priorityLoggers[LoggerPriority.INFO] =  {s, s2 -> Log.i(s, s2) }
        priorityLoggers[LoggerPriority.WARNING] = {s, s2 -> Log.w(s, s2) }
        priorityLoggers[LoggerPriority.ERROR] =  {s, s2 -> Log.e(s, s2) }
    }

    override fun logMessage(
        tag: String?, priority: LoggerPriority,
        message: LoggerMessage
    ) {
        val loggerTag = tag ?: DEFAULT_TAG
        val logger = priorityLoggers[priority]

        if (logger != null) logger(loggerTag, message.joinedString())
    }

    override fun sendUserFeedback(tag: String?, message: String, uid: String) {
        Logger.tag(DEFAULT_TAG).w(CodeRef.x(), message)
    }

    companion object {
        private val DEFAULT_TAG = "LogcatLoggerPlugin"
    }
}