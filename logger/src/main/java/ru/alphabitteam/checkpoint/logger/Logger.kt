package ru.alphabitteam.checkpoint.logger

/**
 * @author Yuri Zigunov (fly1232008@yandex.ru)
 * @since 26.05.2019.
 */
class Logger {

    companion object {

        private  val IMPLEMENTATION = LoggerImplementation()

        @JvmStatic
        fun tag(tag: String): LoggerImplementation {
            return IMPLEMENTATION.tag(tag)
        }

        @JvmStatic
        fun installPlugin(loggerPlugin: LoggerPlugin) {
            IMPLEMENTATION.installPlugin(loggerPlugin)
        }

        @JvmStatic
        fun installPlugins(vararg loggerPlugins: LoggerPlugin) {
            IMPLEMENTATION.installPlugins(*loggerPlugins)
        }

        @JvmStatic
        fun removePlugin(loggerPlugin: LoggerPlugin) {
            IMPLEMENTATION.removePlugin(loggerPlugin)
        }

        @JvmStatic
        fun removeAllPlugins() {
            IMPLEMENTATION.removeAllPlugins()
        }

        @JvmStatic
        fun v(message: String) {
            IMPLEMENTATION.v(message)
        }

        @JvmStatic
        fun v(reference: CodeRef, message: String) {
            IMPLEMENTATION.v(reference, message)
        }

        @JvmStatic
        fun d(message: String) {
            IMPLEMENTATION.d(message)
        }

        @JvmStatic
        fun d(reference: CodeRef, message: String) {
            IMPLEMENTATION.d(reference, message)
        }

        @JvmStatic
        fun i(message: String) {
            IMPLEMENTATION.i(message)
        }

        @JvmStatic
        fun i(reference: CodeRef, message: String) {
            IMPLEMENTATION.i(reference, message)
        }

        @JvmStatic
        fun i(reference: CodeRef, error: Throwable, message: String) {
            IMPLEMENTATION.i(reference, error, message)
        }

        @JvmStatic
        fun i(reference: CodeRef, error: Throwable) {
            IMPLEMENTATION.i(reference, error)
        }

        @JvmStatic
        fun w(reference: CodeRef, message: String) {
            IMPLEMENTATION.w(reference, message)
        }

        @JvmStatic
        fun w(reference: CodeRef, error: Throwable, message: String) {
            IMPLEMENTATION.w(reference, error, message)
        }

        @JvmStatic
        fun w(reference: CodeRef, error: Throwable) {
            IMPLEMENTATION.w(reference, error)
        }

        @JvmStatic
        fun e(reference: CodeRef, message: String) {
            IMPLEMENTATION.e(reference, message)
        }

        @JvmStatic
        fun e(reference: CodeRef, error: Throwable, message: String) {
            IMPLEMENTATION.e(reference, error, message)
        }

        @JvmStatic
        fun e(reference: CodeRef, error: Throwable) {
            IMPLEMENTATION.e(reference, error)
        }

        @JvmStatic
        fun sendUserFeedback(tag: String, message: String, uid: String) {
            IMPLEMENTATION.sendUserFeedback(tag, message, uid)
        }
    }

}