package ru.alphabitteam.checkpoint.logger

/**
 * @author Yuri Zigunov (fly1232008@yandex.ru)
 * @since 26.05.2019.
 */
class LoggerImplementation {

    private val pluginSet = PluginSet()
    private val tag = ThreadLocal<String>()
    private val messagePool = loggerMessagePool()

    fun installPlugin(loggerPlugin: LoggerPlugin) {
        pluginSet.installPlugin(loggerPlugin)
    }

    fun installPlugins(vararg loggerPlugins: LoggerPlugin) {
        pluginSet.installPlugins(*loggerPlugins)
    }

    fun removePlugin(loggerPlugin: LoggerPlugin) {
        pluginSet.removePlugin(loggerPlugin)
    }

    fun removeAllPlugins() {
        pluginSet.removeAllPlugins()
    }

    fun tag(tag: String): LoggerImplementation {
        this.tag.set(tag)
        return this
    }

    fun v(message: String) {
        val loggerMessage = messagePool.borrow() ?: return
        loggerMessage.update(message, null, null)
        pluginSet.takeLogMessage(removeTag(), LoggerPriority.VERBOSE, loggerMessage)
        messagePool.giveBack(loggerMessage)
    }

    fun v(reference: CodeRef, message: String) {
        val loggerMessage = messagePool.borrow() ?: return
        loggerMessage.update(message, reference, null)
        pluginSet.takeLogMessage(removeTag(), LoggerPriority.VERBOSE, loggerMessage)
        messagePool.giveBack(loggerMessage)
    }

    fun d(message: String) {
        val loggerMessage = messagePool.borrow() ?: return
        loggerMessage.update(message, null, null)
        pluginSet.takeLogMessage(removeTag(), LoggerPriority.DEBUG, loggerMessage)
        messagePool.giveBack(loggerMessage)
    }

    fun d(reference: CodeRef, message: String) {
        val loggerMessage = messagePool.borrow() ?: return
        loggerMessage.update(message, reference, null)
        pluginSet.takeLogMessage(removeTag(), LoggerPriority.DEBUG, loggerMessage)
        messagePool.giveBack(loggerMessage)
    }

    fun i(message: String) {
        val loggerMessage = messagePool.borrow() ?: return
        loggerMessage.update(message, null, null)
        pluginSet.takeLogMessage(removeTag(), LoggerPriority.INFO, loggerMessage)
        messagePool.giveBack(loggerMessage)
    }

    fun i(reference: CodeRef, message: String) {
        val loggerMessage = messagePool.borrow() ?: return
        loggerMessage.update(message, reference, null)
        pluginSet.takeLogMessage(removeTag(), LoggerPriority.INFO, loggerMessage)
        messagePool.giveBack(loggerMessage)
    }

    fun i(reference: CodeRef, error: Throwable, message: String) {
        val loggerMessage = messagePool.borrow() ?: return
        loggerMessage.update(message, reference, error)
        pluginSet.takeLogMessage(removeTag(), LoggerPriority.INFO, loggerMessage)
        messagePool.giveBack(loggerMessage)
    }

    fun i(reference: CodeRef, error: Throwable) {
        val loggerMessage = messagePool.borrow() ?: return
        loggerMessage.update(null, reference, error)
        pluginSet.takeLogMessage(removeTag(), LoggerPriority.INFO, loggerMessage)
        messagePool.giveBack(loggerMessage)
    }

    fun w(reference: CodeRef, message: String) {
        val loggerMessage = messagePool.borrow() ?: return
        loggerMessage.update(message, reference, null)
        pluginSet.takeLogMessage(removeTag(), LoggerPriority.WARNING, loggerMessage)
        messagePool.giveBack(loggerMessage)
    }

    fun w(reference: CodeRef, error: Throwable, message: String) {
        val loggerMessage = messagePool.borrow() ?: return
        loggerMessage.update(message, reference, error)
        pluginSet.takeLogMessage(removeTag(), LoggerPriority.WARNING, loggerMessage)
        messagePool.giveBack(loggerMessage)
    }

    fun w(reference: CodeRef, error: Throwable) {
        val loggerMessage = messagePool.borrow() ?: return
        loggerMessage.update(null, reference, error)
        pluginSet.takeLogMessage(removeTag(), LoggerPriority.WARNING, loggerMessage)
        messagePool.giveBack(loggerMessage)
    }

    fun e(reference: CodeRef, message: String) {
        val loggerMessage = messagePool.borrow() ?: return
        loggerMessage.update(message, reference, null)
        pluginSet.takeLogMessage(removeTag(), LoggerPriority.ERROR, loggerMessage)
        messagePool.giveBack(loggerMessage)
    }

    fun e(reference: CodeRef, error: Throwable, message: String) {
        val loggerMessage = messagePool.borrow() ?: return
        loggerMessage.update(message, reference, error)
        pluginSet.takeLogMessage(removeTag(), LoggerPriority.ERROR, loggerMessage)
        messagePool.giveBack(loggerMessage)
    }

    fun e(reference: CodeRef, error: Throwable) {
        val loggerMessage = messagePool.borrow() ?: return
        loggerMessage.update(null, reference, error)
        pluginSet.takeLogMessage(removeTag(), LoggerPriority.ERROR, loggerMessage)
        messagePool.giveBack(loggerMessage)
    }

    fun sendUserFeedback(tag: String, message: String, uid: String) {
        pluginSet.sendUserFeedback(tag, message, uid)
    }

    private fun removeTag(): String? {
        val tag = this.tag.get() ?: return null
        this.tag.remove()
        return tag
    }

    private fun loggerMessagePool(): ObjectPool<LoggerMessage> {
        return object : ObjectPool<LoggerMessage>() {
            override fun createNewObject(): LoggerMessage {
                return LoggerMessage()
            }
        }
    }
}