package ru.alphabitteam.checkpoint.logger

import android.text.TextUtils
import java.io.PrintWriter
import java.io.StringWriter

/**
 * @author Yuri Zigunov (fly1232008@yandex.ru)
 * @since 26.05.2019.
 */
class LoggerMessage @JvmOverloads constructor(
    private var text: String? = null,
    private var reference: CodeRef? = null,
    private var error: Throwable? = null
) {
    private var joinedString: String? = null

    init {
        this.joinedString = null
    }

    fun update(
        text: String?,
        reference: CodeRef?,
        error: Throwable?
    ) {
        this.text = text
        this.reference = reference
        this.error = error
        this.joinedString = null
    }

    fun text(): String? {
        return text
    }

    fun reference(): CodeRef? {
        return reference
    }

    fun error(): Throwable? {
        return error
    }

    fun joinedString(): String {
        if (joinedString.isNullOrEmpty()) {
            joinedString = joinAllFields()
        }
        return joinedString as String
    }

    override fun toString(): String {
        return joinedString()
    }

    private fun joinAllFields(): String {
        val joinedString: StringBuilder = java.lang.StringBuilder()
        var needSeparator = false

        if (!text.isNullOrEmpty()) {
            joinedString.append(text)
            needSeparator = true
        }

        if (reference != null && !TextUtils.isEmpty(reference!!.codeReference())) {
            if (needSeparator) {
                joinedString.append("\n")
            }
            joinedString.append(reference!!.codeReference())
            needSeparator = true
        }

        if (error != null) {
            if (needSeparator) {
                joinedString.append("\n")
            }
            joinedString.append(stackTraceText(error!!))
        }

        return joinedString.toString()
    }

    private fun stackTraceText(throwable: Throwable): String {
        val sw = StringWriter()
        val pw = PrintWriter(sw)
        throwable.printStackTrace(pw)
        pw.flush()
        return sw.toString()
    }
}