package ru.alphabitteam.checkpoint.logger

/**
 * @author Yuri Zigunov (fly1232008@yandex.ru)
 * @since 26.05.2019.
 */
abstract class LoggerPlugin(var isEnabled: Boolean, private var minLogPriority: LoggerPriority) {

    fun takeLogMessage(
        tag: String?,
        priority: LoggerPriority,
        message: LoggerMessage
    ) {
        if (!isEnabled) {
            return
        }
        if (priority.moreVerboseThan(minLogPriority)) {
            return
        }
        logMessage(tag, priority, message)
    }

    fun takeUserFeedback(
        tag: String?,
        message: String,
        userId: String
    ) {
        if (!isEnabled) {
            return
        }
        sendUserFeedback(tag, message, userId)
    }

    fun minLogPriority(): LoggerPriority {
        return minLogPriority
    }

    fun setMinLogPriority(minLogPriority: LoggerPriority) {
        this.minLogPriority = minLogPriority
    }

    protected abstract fun logMessage(
        tag: String?,
        priority: LoggerPriority,
        message: LoggerMessage
    )

    protected abstract fun sendUserFeedback(
        tag: String?,
        message: String,
        uid: String
    )
}
