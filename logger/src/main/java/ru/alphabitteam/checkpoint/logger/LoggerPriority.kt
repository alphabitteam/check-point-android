package ru.alphabitteam.checkpoint.logger

import android.util.Log

/**
 * @author Yuri Zigunov (fly1232008@yandex.ru)
 * @since 26.05.2019.
 */
enum class LoggerPriority private constructor(private val androidPriority: Int) {

    VERBOSE(Log.VERBOSE),
    DEBUG(Log.DEBUG),
    INFO(Log.INFO),
    WARNING(Log.WARN),
    ERROR(Log.ERROR);

    fun androidPriority(): Int {
        return androidPriority
    }

    fun moreVerboseThan(otherPriority: LoggerPriority): Boolean {
        return androidPriority < otherPriority.androidPriority()
    }

    companion object {

        @JvmStatic
        fun valueOf(androidPriority: Int): LoggerPriority? {
            for (value in values()) {
                if (value.androidPriority == androidPriority) {
                    return value
                }
            }
            return null
        }
    }

}
