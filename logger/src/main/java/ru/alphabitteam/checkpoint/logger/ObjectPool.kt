package ru.alphabitteam.checkpoint.logger

import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue

/**
 * @author Yuri Zigunov (fly1232008@yandex.ru)
 * @since 26.05.2019.
 */
internal abstract class ObjectPool<T> {

    private val objects: Queue<T>

    init {
        this.objects = ConcurrentLinkedQueue()
    }

    fun borrow(): T? {
        var `object`: T? = objects.poll()
        if (`object` == null) {
            `object` = createNewObject()
        }
        return `object`
    }

    fun giveBack(`object`: T) {
        objects.offer(`object`)
    }

    abstract fun createNewObject(): T
}