package ru.alphabitteam.checkpoint.logger

import java.util.*

/**
 * @author Yuri Zigunov (fly1232008@yandex.ru)
 * @since 26.05.2019.
 */
internal class PluginSet : LoggerPlugin(true, LoggerPriority.VERBOSE) {

    private val pluginList = mutableListOf<LoggerPlugin>()
    @Volatile
    private var pluginArray = EMPTY_PLUGIN_ARRAY

    override fun logMessage(tag: String?, priority: LoggerPriority,
                            message: LoggerMessage) {
        val plugins = pluginArray
        for (plugin in plugins) {
            plugin?.takeLogMessage(tag, priority, message)
        }
    }

    public override fun sendUserFeedback(tag: String?, message: String, uid: String) {
        val plugins = pluginArray
        for (plugin in plugins) {
            plugin?.takeUserFeedback(tag, message, uid)
        }
    }

    fun installPlugin(loggerPlugin: LoggerPlugin) {
        if (loggerPlugin === this) {
            throw IllegalArgumentException("Cannot install Logger into itself")
        }
        synchronized(pluginList) {
            pluginList.add(loggerPlugin)
            pluginArray = pluginList.toTypedArray()
        }
    }

    fun installPlugins(vararg loggerPlugins: LoggerPlugin) {
        for (loggerPlugin in loggerPlugins) {
            if (loggerPlugin === this) {
                throw IllegalArgumentException("Cannot install Logger into itself")
            }
        }
        synchronized(pluginList) {
            Collections.addAll(pluginList, *loggerPlugins)
            pluginArray = pluginList.toTypedArray()
        }
    }

    fun removePlugin(loggerPlugin: LoggerPlugin) {
        synchronized(pluginList) {
            if (!pluginList.remove(loggerPlugin)) {
                throw IllegalArgumentException("Cannot remove plugin which is not installed: $loggerPlugin")
            }
            pluginArray = pluginList.toTypedArray()
        }
    }

    fun removeAllPlugins() {
        synchronized(pluginList) {
            pluginList.clear()
            pluginArray = EMPTY_PLUGIN_ARRAY
        }
    }

    companion object {

        private val EMPTY_PLUGIN_ARRAY = arrayOfNulls<LoggerPlugin>(0)
    }
}